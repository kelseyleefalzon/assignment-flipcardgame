﻿﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine.Audio;

public class Lists : MonoBehaviour
{
    public GameObject BackofCardLogo;

    float initialY = 2.50f; //This is the starting point of y where the cards will display
    float initialX = 7.55f; // This is the starting point starting point of x where the card will display
    float x;
    const float offset = 1.35f; //This is the space between the images
    
    int count = 0;

    bool runOnce = false;
    bool listFull = false;
    bool listFull2 = false;

    // to keep the sprites
    List<GameObject> Sprites = new List<GameObject>();
    // to update with used Sprites
    List<GameObject> showedSprites = new List<GameObject>();
    List<GameObject> Sprites2 = new List<GameObject>();
    List<GameObject> showedSprites2 = new List<GameObject>();

    void SpawnRandomSprites()
    {
        //This will get a random number from 0 - 3
        int location = Random.Range(0, Sprites.Count);

        //This retrieves the object at position [location] from listOfSprites
        GameObject myRandomObject = Sprites[location];

        // if the object is not in second list usedSprites
        if (showedSprites.Contains(myRandomObject) == false)
        {
            showedSprites.Add(myRandomObject);

            Vector3 spawnPoint = new Vector3(x, initialY, 0);

            //
            GameObject mySprite = Instantiate(myRandomObject, spawnPoint, transform.rotation) as GameObject;

            //instantiate the backofcards on the same space
            GameObject BackOfcard = Instantiate(BackofCardLogo, spawnPoint, transform.rotation) as GameObject;

            //The tag hat has been given to the card
            BackOfcard.tag = "card";

            //This will set the Sprite as the parent of the backofcards
            BackOfcard.transform.parent = mySprite.transform;

            //This makes the backofcards invisible
            BackOfcard.GetComponent<SpriteRenderer>().enabled = false;

            x = x - offset; // the same as x+=offset

            count++;

            if (count ==12)
            {
                //skip a line
                initialY -= offset;
                // set x to initial
                x = initialX;
            }

            //if all 18 images displayed
            if (count == 18)
            {
                runOnce = true;
            }

            if (showedSprites.Count == Sprites.Count)
            {
                listFull = true;
            }
        }
    }


    void SpawnRandomSprites2()
    {
        int location = Random.Range(0, Sprites2.Count);
        GameObject myRandomObject = Sprites2[location];

        // if object is not in second list usedSprites

        if (showedSprites2.Contains(myRandomObject) == false)
        {
            showedSprites2.Add(myRandomObject);          

            Vector3 spawnPoint = new Vector3(x, initialY, 0);

            // 
            GameObject mySprite2 = Instantiate(myRandomObject, spawnPoint, transform.rotation) as GameObject;

            //instantiate the backofcards on the same space
            GameObject BackOfcard = Instantiate(BackofCardLogo, spawnPoint, transform.rotation) as GameObject;

            //The tag that has been given
            BackOfcard.tag = "card";

            //This will set the Sprite as the parent of the backofcards
            BackOfcard.transform.parent = mySprite2.transform;

            //This makes the backofcards invisible
            BackOfcard.GetComponent<SpriteRenderer>().enabled = false;


            x = x - offset; // the same as x+=offset

            count++;

            if (count == 24)
            {
                initialY -= offset;
                x = initialX;
            }       

            else if (count == 36)
            {
                initialY -= offset;
                x = initialX;
                runOnce = true;
            }

            if (showedSprites2.Count == Sprites2.Count)
            {
                listFull2 = true;
            }

        }
    }


    IEnumerator HideAllCards()
    {
        yield return new WaitForSeconds(3f);
        GameObject[] logos = GameObject.FindGameObjectsWithTag("card");
        Debug.Log(logos[0].name);

        foreach (GameObject tempLogo in logos)
        {
            tempLogo.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    // Use this for initialization
    void Start()
    {
        x = initialX;

        // This will load all images in Assets/Resources/Prefabs in my listOfSprites
        Sprites.AddRange(Resources.LoadAll<GameObject>("Prefabs"));
        Sprites2.AddRange(Resources.LoadAll<GameObject>("Prefabs"));

        do
        {
            SpawnRandomSprites();
        }

        while (listFull == false);

        do
        {
            SpawnRandomSprites2();
        }
        while (listFull2 == false);
        StartCoroutine(HideAllCards());
    }


    // Update is called once per frame
    void Update()
    {
    }
}

