﻿using UnityEngine;
using System.Collections;

public class Mycards : MonoBehaviour {

    public AudioClip Win;
    public AudioClip Cardflip;
    public AudioClip WrongBuzzer;
    AudioSource aso;
    AudioSource audio;

    IEnumerator Wait2Seconds()
    {
        if (Script.card1 != Script.card2)
        {
            //This will log if the cards do not match 
            Debug.Log("Cards don't match");
            yield return new WaitForSeconds(2f);

            //This will get the back of card in front after 3 seconds
            GetComponent<SpriteRenderer>().sortingOrder += 3;

        }

        else
        {
            //This will log if the cards match
            Debug.Log(" Cards match");
            Script.click = 0;
        }
    }

    void OnMouseDown()
    {
        if (Script.click == 0) // If the first card is clicked
        {
            //This will send backofcard two layers back
            GetComponent<SpriteRenderer>().sortingOrder -= 2;
            //This will get the name of the card that is clicked
            Script.card1 = this.transform.parent.name;
            Script.click++;
            aso.Play();
            Debug.Log("First Card is Clicked");
        }

        else if (Script.click == 1) //If the second card is clicked
        {
            GetComponent<SpriteRenderer>().sortingOrder -= 2;
            //This will get the name of the second card that is clicked
            Script.card2 = this.transform.parent.name;
            StartCoroutine(Wait2Seconds());
            aso.Play();
            Debug.Log("Second Card is clicked");
        }
    }
    // Use this for initialization
    void Start()
    {
        aso = gameObject.AddComponent<AudioSource>();
        aso.clip = Cardflip;
    }
        // Update is called once per frame
        void Update()
    {

    }
}

