﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour
{

    GUIStyle myStyle = new GUIStyle();
    public static int score = 0;
    public GUISkin mySkin;

    void OnGUI()
    {
        //myStyle.fontSize = 35;
        //myStyle.normal.textColor = Color.yellow;
        GUI.skin = mySkin;

        GUI.Label(new Rect(100, 0, 200, 100), "Score: " + score);
    }

    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }
}
